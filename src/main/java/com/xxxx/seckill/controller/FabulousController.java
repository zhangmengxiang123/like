package com.xxxx.seckill.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 点赞接口实现
 *
 * @author zmx.
 * @create 2022/01/19
 */
@Api(value = "点赞",tags = {"点赞接口实现"})
@RestController
@Slf4j
public class FabulousController {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 点赞
     */
    @ApiOperation(value = "点赞",notes = "点赞",httpMethod = "GET")
    @GetMapping(value = "/dolike",produces = "application/json;charset=utf-8")
    public String dolike(int postid, int userid) {
        String result = "";
        try {
            String key = "like:" + postid;
            Long object = this.redisTemplate.opsForSet().add(key, userid);
            if (object == 1) {
                result = "点赞成功";
            } else {
                result = "你已重复点赞！";
            }
            log.info("查询结果：{}", object);
        } catch (Exception e) {
            log.error("exception:", e.getMessage());
        }
        return result;
    }

    /**
     * 取消点赞
     */
    @ApiOperation(value = "取消点赞",notes = "取消点赞",httpMethod = "GET")
    @GetMapping(value = "/undolike",produces = "application/json;charset=utf-8")
    public String undolike(int postid, int userid) {
        String result = "";
        try {
            String key = "like:" + postid;
            Long object = this.redisTemplate.opsForSet().remove(key, userid);
            if (object == 1) {
                result = "取消成功";
            } else {
                result = "你已重复取消点赞！";
            }
            log.info("查询结果：{}", object);
        } catch (Exception e) {
            log.error("exception:", e.getMessage());
        }
        return result;
    }

    /**
     * 查看帖子信息
     * 根据id查询帖子信息，返回点赞总数和是否点赞
     */
    @ApiOperation(value = "查看帖子信息",notes = "查看帖子信息",httpMethod = "GET")
    @GetMapping(value = "/getpost")
    public Map getpost(int postid, int userid) {
        Map map = new HashMap<>();
        String result = "";
        try {
            String key = "like:" + postid;
            Long size = this.redisTemplate.opsForSet().size(key);
            Boolean bo = this.redisTemplate.opsForSet().isMember(key, userid);
            map.put("size", size);
            map.put("isLike", bo);
            log.info("查询结果:{}", bo);
        } catch (Exception e) {
            log.error("exception:", e.getMessage());
        }
        return map;
    }

    /**
     * 查询点赞明细，有那些人
     */
    @ApiOperation(value = "查询点赞明细",notes = "查询点赞明细",httpMethod = "GET")
    @GetMapping(value = "/likedetail")
    public Set likedetail(int postid) {
        Set set = null;
        try {
            String key = "like:" + postid;
            set = this.redisTemplate.opsForSet().members(key);
            log.info("查询结果:{}", set);
        } catch (Exception e) {
            log.error("exception:", e.getMessage());
        }
        return set;
    }

}
